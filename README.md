## Cara menggunakan project
1. Clone project dengan menjalankan perintah berikut.
```
git clone https://gitlab.com/dwikyramadhanh-my-repository/laravel/laravel-notification-telegram-channel
```
2. Pindah ke folder project.
```
cd laravel-notification-telegram-channel
```
3. install composer.
```
composer install
```
4. Buka folder aplikasi dengan text editor, lalu duplicate file .env.example dan rename menjadi .env.
5. Buat database pada DBMS Anda.
6. Buka file .env dan ubah konfigurasi databasenya seperti contoh berikut.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database
DB_USERNAME=root
DB_PASSWORD=
```
* <strong>note = </strong> sesuaikan dengan konfigurasi database Anda.
7. Jalankan migration untuk membuat table.
```
php 
```
8. Buat bot telegram (untuk membuatnya ada di google atau youtube).
9. Tambahkan token bot telegram pada file .env.
```
TELEGRAM_BOT_TOKEN=your_telegram_bot_token
```
10. Tambahkan username bot telegram pada file app\Notifications\PostPublished.php.
```
public function toTelegram($post)
{
    return TelegramMessage::create()->to('@your_bot_telegram')
    ->content("$post->title - http://localhost:8000/home/"  . $post->slug);
}
``` 
11. Generate key app.
```
php artisan key:generate
```
12. Jalankan project.
```
php artisan config:cache
php artisan serve
npm run dev
```
13. Url project.
```
http://localhost:8000/home
```

