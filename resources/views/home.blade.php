@extends('layouts.app', ['title' => 'Laravel Telegram Notification'])

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <form action="{{ route('posts.store') }}" method="POST">
        @csrf
        <div class="mb-3">
          <label for="title" class="form-label">Title</label>
          <input type="text" name="title" class="form-control" id="title">
        </div>
        <button type="submit" class="btn btn-md btn-primary">Save</button>
      </form>
    </div>
  </div>
</div>
@endsection
